Project Name: UR5-Robot-Arm

Description: Contains source codes necessary for solving the inverse
kinematic problem of a 6 DOF UR5 robot manipulator

Author: Swagat Kumar (swagat.kumar@gmail.com)

License: Available under GPL Version 3.0 or later

----------------------------------

Dependencies:

- GNU/Linux (Tested on Ubuntu 14.04 64bit LTS)
- OpenCV
- Gnuplot_CI : https://gitlab.com/swagat-kumar/gnuplot_ci
- G++ / GCC

Compilation instruction: Run the CMakeList.txt file to build
executable



*Folders:*

./cpp  : Contains C++ source codes

./maxima : contains maxima source codes

./docs : contains necessary documentation




 *Updates*

---------------------------

May 18, 2015

- Robot Configuration is tested
- Analytical Jacobian is tested. It seems to work fine. 
- Look into the file "fk_test.cpp"
- To do: Test Geometric Jacobian


May 22, 2015

- Geometric Jacobian is tested
- Visual Servoing is tested
- Look into the file "vsdemo.cpp"
- To DO: Real-time test on UR5


